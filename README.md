# swp-autonome-fahrzeuge-sose-2018

---
### Task (as given, in german)

Baustellen, Unfälle, umgefallene Bäume oder andere dynamisch auftretende Verkehrsbeeinträchtigungen erfordern erhöhte Aufmerksamkeit, stellen eine Unfallgefahr dar und können zu Stau führen. Autos können als mobiler Sensorknoten die aktuellen Gegebenheiten des Engpasses messen und an andere Teilnehmer upstream kommunizieren. Implementiere zwei Varianten: eine Methode, die ein Informationspaket lokal an (oder besser vor) der Unfall-/Baustelle platziert, und eine Methode, mit der Teilnehmer Informationspakete upstream versenden, so weit, wie die Lebenszeit des Pakets zulässt. Wie können andere Teilnehmer diese Informationen nutzen?

---

### Documentation

Please refer to the documentation in [the Wiki section](https://git.imp.fu-berlin.de/adrianghc/swp-autonome-fahrzeuge-sose-2018/wikis/home)

--- 

### Some development notes

#### Units of measurement in SUMO

* Simulation step = 300 milliseconds (this was configured in `main.py`, explanation [here](http://sumo.dlr.de/wiki/Simulation/Basic_Definition#Defining_the_Time_Step_Length))
    * Note that we are also simulating a lot faster than "real time",
      however all of this is configurable
* Time: milliseconds
* Distance: meters
* Speed: m/s

#### Profiling

If you would like to profile the application and you are running unix based OS, you could simply use
the `src/profiling.sh` script. Otherwise, feel free to give it a look and use the same 
functionality in a terminal of your choice.


#### Configuration

##### Vehicles

Using the vehicle factory, we have several types of vehicles which can be used for different
purposes (e.g. autonomous cars, cars which have the sole purpose of making an accident, etc.)

For the car type definition, open the `<scenario>.rou.xml` file and change the `id`
property of the `vType` as follows:

1. autonomous cars: `auto_car`; Uses `AutoCar` class
1. autonomous cars for information transmission demo: `auto_car_color_demo`;
    Uses `AutoCarColorDemo` class
1. crazy cars: `crazy_car`; Uses `CrazyCar` class
1. autonomous crazy cars: `auto_crazy_car`; Uses `AutoCrazyCar` class
1. communication initiator cars: `communication_initiator_car`; 
  Uses `CommunicationInitiatorCar` class
1. autonomous cars with centralized communication: `auto_car_centralized`;
  Uses `AutoCarCcentralized` class

*In case an unrecognized type is given, autonomous car is used*

##### Information Beacons:

For now, just junctions can be used as an information beacons. Of course, this can further 
be extended in the future

###### Junctions

To configure a junction as information beacon, open `netedit`, open the `<scenario>.net.xml`
file, select the junction you
wish to be added as information beacon and change it's id to start with `beacon-<id here>`.
For the centralized communication case, use `beacon-centralized-<id here>`, however
only one beacon should be used in this case. It will act
as the centralized server.

*For debugging and colorful information transmission demo, use `beacon-color-<id here>`*

---
