from itertools import chain

################################################################################
# Templates for callbacks for better docstring documentation
################################################################################


def on_new_information_cb_template(topic, information):
    """
    Template for `on_new_information_cb` callback function.
    """

################################################################################
# END TEMPLATES
################################################################################


class InformationStorage:
    """
    Stores all the information an autonomous vehicle is aware of.
    Is bound to `MessageScheduler`, so the information of newly scheduled or unscheduled messages is being
    inserted/removed from this store as well.

    Make sure you call `InformationStorage::is_new()` before sending a message about a newly discovered situation.
    Further, register `on_new_information_cb` to be notified when new information enters the store.
    """

    def __init__(self, message_scheduler, on_new_information_cb=on_new_information_cb_template):
        self._message_scheduler = message_scheduler
        self._message_scheduler.add_on_insert_callback(self.on_insert)
        self._message_scheduler.add_on_remove_callback(self.on_remove)
        self.store = {}
        self.on_new_information_cb = on_new_information_cb

    def insert(self, topic, information):
        """
        Adds an `IInformation` object to the store. Calls `on_new_information_cb` to notify store creator
        of new information.

        :param InformationSharing.Message.MessageTopic topic: Topic the information object corresponds to
        :param InformationKnowledge.Information.IInformation information: Information to be added to the store
        :return boolean: True if the information is new and therefore didn't exist in the store before
        """

        if topic not in self.store:
            self.store[topic] = {}

        if self.is_new(topic, information):
            if information.lane not in self.store[topic]:
                self.store[topic][information.lane] = []

            self.store[topic][information.lane].append(information)
            self.on_new_information_cb(topic, information)
            return True

        return False

    def remove(self, topic, information):
        """
        Removes an `IInformation` object from the store.

        :param InformationSharing.Message.MessageTopic topic: Topic the information object corresponds to
        :param InformationKnowledge.Information.IInformation information: Information to be removed from the store
        :return boolean: True if the information was in the store and could be removed
        """
        if topic in self.store and information.lane in self.store[topic] \
           and information in self.store[topic][information.lane]:
            self.store[topic][information.lane].remove(information)
            return True

        return False

    def is_new(self, topic, information):
        """
        Checks whether an `IInformation` object is in the store.

        :param InformationSharing.Message.MessageTopic topic: Topic the information object corresponds to
        :param InformationKnowledge.Information.IInformation information: Information for which we want to do the check
        :return boolean: True if no similar information is already within the message store
        """
        if topic not in self.store:
            return True

        return information.lane not in self.store[topic] or information not in self.store[topic][information.lane]

    def on_remove(self, message):
        """
        Called from MessageScheduler when a message is no longer relevant.
        """
        self.remove(message.topic, message.data)

    def on_insert(self, message):
        """
        Called from MessageScheduler when a new relevant message is available.
        """
        self.insert(message.topic, message.data)

    def get_info_for_topic(self, topic):
        """
        Returns the information objects corresponding to the given topic.
        """
        if topic in self.store:
            return list(chain(*self.store[topic].values()))

        return []
