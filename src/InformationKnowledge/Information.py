class IInformation:
    """
    Interface to define what an information instance can store.
    """

    def __init__(self, location=None, edge=None, lane=None, avg_speed=None):
        self.id_ = id(self)
        self.event_location = location
        self.edge = edge
        self.lane = lane
        self.avg_speed = avg_speed

        # What more is relevant at the moment?


class AccidentInformation(IInformation):
    def __eq__(self, other):
        """

        :param IInformation other: The `IInformation` to be compared with
        :return boolean: Whether self and other equal
        """

        # NOTE: when required, add further properties which must
        #       match in order for two information instances to be equal.
        return self.lane == other.lane
