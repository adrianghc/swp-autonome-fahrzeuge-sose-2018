import pandas as pd
from Logging.Logging import Logger


class _CentralizedMessageLoggerStorage:
    def __init__(self):
        self.current_timestamp = None
        self.to_server_count = 0
        self.from_server_count = 0
        self.total_count = 0

    def export_log(self):
        """
        Returns a pandas data frame in the log format.
        Also resets count data.
        """

        data_frame = pd.DataFrame({
            'time': [self.current_timestamp],
            'to_server_count': [self.to_server_count],
            'from_server_count': [self.from_server_count],
            'total_count': [self.total_count],
        })

        self.to_server_count = 0
        self.from_server_count = 0
        self.total_count = 0

        return data_frame


class CentralizedMessageLogger(Logger):
    """
    Logger for the case of centralized communication. Format:

    [
        [...], // this is for one simulation step
        [time, to_server_count, from_server_count, total_count],
    ]
    """

    log_data_frames = []
    _storage = _CentralizedMessageLoggerStorage()

    @classmethod
    def log_message_to_server(cls, time):
        """
        Call when peer sends a message to server (either location update or accident notification).

        :param int time: in ms
        """
        if not cls._should_log(None):
            return

        cls._handle_timestamp(time)
        cls._storage.to_server_count += 1
        cls._storage.total_count += 1

    @classmethod
    def log_message_from_server(cls, time):
        """
        Call when server sends push notification to a peer.

        :param int time: in ms
        """
        if not cls._should_log(None):
            return

        cls._handle_timestamp(time)
        cls._storage.from_server_count += 1
        cls._storage.total_count += 1

    @classmethod
    def _handle_timestamp(cls, time):
        """
        If the timestamp is different from the previously saved in storage,
        export a new log frame and reset counters (2. is handled within export method itself)
        :param time: The new timestamp
        """
        if time != cls._storage.current_timestamp:
            cls._storage.current_timestamp = time
            cls.log_data_frames.append(cls._storage.export_log())

    ############################################################################
    # Implement Logger contract
    ############################################################################

    @classmethod
    def write_logs(cls):
        """"""
        print('Writing centralized message logs...')
        if not cls.log_data_frames:  # empty
            return

        # add headers for first data frame
        cls.log_data_frames[0].to_csv(cls.get_log_file(), index=False)

        # then add all others, without headers, append mode
        for df in cls.log_data_frames[1:]:
            df.to_csv(cls.get_log_file(), header=False, mode='a', index=False)

        print('Wrote centralized message logs!')

    @staticmethod
    def get_log_type():
        """"""
        return 'centralized_message_log'
