import pandas as pd
from Logging.Logging import Logger


class MessageLogger(Logger):
    """
    Logs messaging relevant information for statistical analysis.

    Should log:

    - How many messages each information peer is currently distributing
    - How many messages a peer sent / received
    - Transmission range

    [
        [time(in ms), peer_id, transmission_range, messages_sent, messages_received, messages_distributing],
    ]
    """

    peers_storage = {}
    log_data_frames = []
    current_simulation_time = None

    @classmethod
    def log_peer(cls, simulation_time, peer, messages_sent,
                 messages_received, messages_distributing):
        """
        Log message peer for a time step.

        :param int simulation_time: Simulation time in ms
        :param InformationSharing.InformationPeer.InformationPeer peer: The message peer object
        :param string peer_id: The message peer id
        :param int transmission_range: The transmission range in meters
        :param int messages_sent: The number of messages sent
        :param int messages_received: The number of messages received
        :param int messages_distributing: The number of messages scheduled for distribution
        """
        if not cls._should_log(peer):
            return

        peer_id = peer.get_id()
        transmission_range = peer.get_transmission_range()

        if cls.current_simulation_time is None:
            cls.current_simulation_time = simulation_time
        elif cls.current_simulation_time != simulation_time:
            cls._save_data_frames()
            cls.peers_storage.clear()
            cls.current_simulation_time = simulation_time

        if peer_id not in cls.peers_storage:
            cls.peers_storage[peer_id] = _PeerLog(simulation_time, peer_id, transmission_range,
                                                  messages_sent, messages_received, messages_distributing)
        else:
            cls.peers_storage[peer_id].increment(messages_sent, messages_received, messages_distributing)

    @classmethod
    def extend_peer_log(cls, peer, messages_sent=0, messages_received=0, messages_distributing=0):
        """
        Extend logs of peer for time step. In case the peer was already logged, be the peer e.g.
        send another message afterwards in the same timestep.

        Arguments same as for `MessageLogging::log_peer()`.

        :param InformationSharing.InformationPeer.InformationPeer peer: The message peer object
        :param int messages_sent: The number of messages sent
        :param int messages_received: The number of messages received
        :param int messages_distributing: The number of messages scheduled for distribution
        """
        if not cls._should_log(peer):
            return

        peer_id = peer.get_id()

        assert peer_id in cls.peers_storage, 'Cannot extend logs if peer was not already logged this time step!'
        cls.peers_storage[peer_id].increment(messages_sent, messages_received, messages_distributing)

    @classmethod
    def _save_data_frames(cls):
        """"""
        data_frames = list(map(lambda x: x.export(), cls.peers_storage.values()))
        cls.log_data_frames.extend(data_frames)

    @classmethod
    def _should_log(cls, peer):
        """"""

        from InformationBeacons.InformationBeaconCentralized import InformationBeaconCentralized
        from Vehicles.AutoCarCentralized import AutoCarCentralized

        centralized = isinstance(peer, InformationBeaconCentralized) or isinstance(peer, AutoCarCentralized)
        return super()._should_log(cls) and not centralized

    ############################################################################
    # Implement Logger contract
    ############################################################################

    @classmethod
    def write_logs(cls):
        """"""
        print('Writing message logs...')
        if not cls.log_data_frames:  # empty
            return

        # add headers for first data frame
        cls.log_data_frames[0].to_csv(cls.get_log_file(), index=False)

        # then add all others, without headers, append mode
        for df in cls.log_data_frames[1:]:
            df.to_csv(cls.get_log_file(), header=False, mode='a', index=False)

        print('Wrote message logs!')

    @staticmethod
    def get_log_type():
        """"""
        return 'message_log'


class _PeerLog:
    def __init__(self, simulation_time, peer_id, transmission_range, messages_sent,
                 messages_received, messages_distributing):
        """
        Arguments same as for `MessageLogging::log_peer()`.

        :param int simulation_time: Simulation time in ms
        :param InformationSharing.InformationPeer.InformationPeer peer: The message peer object
        :param string peer_id: The message peer id
        :param int transmission_range: The transmission range in meters
        :param int messages_sent: The number of messages sent
        :param int messages_received: The number of messages received
        :param int messages_distributing: The number of messages scheduled for distribution
        """
        self.simulation_time = simulation_time
        self.peer_id = peer_id
        self.transmission_range = transmission_range
        self.messages_sent = messages_sent
        self.messages_received = messages_received
        self.messages_distributing = messages_distributing

    def increment(self, messages_sent, messages_received, messages_distributing):
        """
        Increment message counting with given parameters.

        :param int messages_sent: The number of messages sent
        :param int messages_received: The number of messages received
        :param int messages_distributing: The number of messages scheduled for distribution
        """
        self.messages_sent += messages_sent
        self.messages_received += messages_received
        self.messages_distributing += messages_distributing

    def export(self):
        """
        Returns the logged values in a pandas dataframe.
        """
        return pd.DataFrame({
            'simulation_time': [self.simulation_time],
            'peer_id': [self.peer_id],
            'transmission_range': [self.transmission_range],
            'messages_sent': [self.messages_sent],
            'messages_received': [self.messages_received],
            'messages_distributing': [self.messages_distributing],
        })
