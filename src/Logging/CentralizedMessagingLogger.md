### Details on how the traffic of a centralized system can be analyzed.

1. As a new obstacle is discovered, all actors within the simulation
    receive a push notification, therefore `number of actors` messages
    are being logged as sent at that time
1. Every time a new actor enters the simulation, `number of relevenat news` 
    messages are being logged, as these actors would receive a push notification
    when entering the area
1. We assume a centralized system would require location updates from its peers.
   Therefore, we assume each peer sends it's location once per `15 seconds` and
   log this as message being sent. To do this more efficiently, we log
   message being sent each 3 seconds, but for `number of actors/5`, as this
   better illustrates that usually not all peers will send their location at the
   same time and will smooth out the plot.