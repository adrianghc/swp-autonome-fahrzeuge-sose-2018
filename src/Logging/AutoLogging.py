import pandas as pd
from Logging.Logging import Logger

# TODO: add reroute counter


class AutoLogger(Logger):
    """
    Logger for the logging of information about vehicles for the purpose of statistical analysis of the project.
    The following is being logged here:

    - Total travel time for a vehicle
    - Total wait time for a vehicle
    - Travel & wait time for a vehicle on a particular edge as well as the edge_id

    Logging output:

    [
        [...], // one line contains the information about one vehicle
        [..., ..., [list of information about the traveling on one edge for one vehicle]]
        [travel_time, wait_time, [(start_time, travel_time, wait_time, edge_id, reroute_count)]]
    ]

    If timely flag is enabled, outputs just the edge_travel_info, each on a new line, with vehicle_id.
    """

    TIMELY = True

    # use vehicle id here as key for dictionary

    # for final logging
    log_per_car = {}
    # for intermediate data storage
    storage_per_car = {}

    @classmethod
    def on_auto_departed(cls, vehicle):
        """
        Call when an instance of a vehicle is created.

        :param Vehicles.Vehicle.Vehicle vehicle: `Vehicle` instance
        """

        if not cls._should_log(vehicle):
            # for now we are only logging AutoCar instances
            return

        cls.storage_per_car[vehicle.id_] = _AutoStorage(vehicle.get_time())
        cls.log_per_car[vehicle.id_] = _LogStorage(vehicle.id_)

        pass

    @classmethod
    def on_step(cls, vehicle, edge_id):
        """
        Call on simulation step from vehicle.

        :param Vehicles.Vehicle.Vehicle vehicle: `Vehicle` instance
        :param string edge_id: Id of the edge the vehicle is currently on
        """

        if not cls._should_log(vehicle):
            # for now we are only logging AutoCar instances
            return

        auto_storage = cls.storage_per_car[vehicle.id_]
        new_edge_log = auto_storage.get_edge_travel_info(vehicle.get_time(), edge_id)

        if new_edge_log is not None:
            cls._add_edge_log(vehicle, new_edge_log)

    @classmethod
    def on_stop_finished(cls, vehicle, time_stopped):
        """
        Call when the vehicle has finished a stop (when its speed is no longer 0).

        :param Vehicles.Vehicle.Vehicle vehicle: `Vehicle` instance
        :param int time_stopped: Time stopped in ms
        """

        if not cls._should_log(vehicle):
            # for now we are only logging AutoCar instances
            return

        auto_storage = cls.storage_per_car[vehicle.id_]
        auto_storage.log_stop(time_stopped)

    @classmethod
    def on_auto_arrived(cls, vehicle):
        """
        Call when a vehicle is leaving the simulation.

        :param Vehicles.Vehicle.Vehicle vehicle: `Vehicle` instance
        """
        if not cls._should_log(vehicle):
            # for now we are only logging AutoCar instances
            return

        auto_storage = cls.storage_per_car[vehicle.id_]
        new_edge_log = auto_storage.get_edge_travel_info(vehicle.get_time(), 'END_OF_MAP_FAKE_EDGE')
        cls._add_edge_log(vehicle, new_edge_log)

    @classmethod
    def on_reroute(cls, vehicle):
        """
        Call when vehicle changes its route (due to new knowledge).

        :param Vehicles.Vehicle.Vehicle vehicle: `Vehicle` instance
        """
        if not cls._should_log(vehicle):
            # for now we are only logging AutoCar instances
            return

        auto_storage = cls.storage_per_car[vehicle.id_]
        auto_storage.reroute_count += 1

    ############################################################################
    # Implement Logger contract
    ############################################################################

    @classmethod
    def write_logs(cls):
        """"""
        print('Writing vehicle logs...')
        logs = cls.log_per_car.values()
        data_frames = list(map(lambda x: x.export_timely() if cls.TIMELY else x.export(), logs))

        if not data_frames:  # empty
            return

        # add headers for first data frame
        data_frames[0].to_csv(cls.get_log_file(), index=False)

        # then add all others, without headers, append mode
        for df in data_frames[1:]:
            df.to_csv(cls.get_log_file(), header=False, mode='a', index=False)

        print('Wrote vehicle logs!')

    @staticmethod
    def get_log_type():
        """"""
        return 'vehicle_log'

    ############################################################################
    # PRIVATE METHODS
    ############################################################################

    @classmethod
    def _should_log(cls, vehicle):
        """
        Helper to decide if the current vehicle should be logged. For now only instances
        of AutoCar are logged, however this could change in the future.

        :param Vehicles.Vehicle.Vehicle vehicle: `Vehicle` instance
        """
        # import here to prevent circular dependency
        from Vehicles.AutoCar import AutoCar

        # for now we are only logging AutoCar instances
        return super()._should_log(cls) and isinstance(vehicle, AutoCar)

    @classmethod
    def _add_edge_log(cls, vehicle, new_edge_log):
        """
        Helper method to add an `_EdgeLogStorage` instance to edge_travel_info.

        :param Vehicles.Vehicle.Vehicle vehicle: `Vehicle` instance
        :param _EdgeLogStorage new_edge_log: `_EdgeLogStorage` instance
        """
        log_per_car = cls.log_per_car[vehicle.id_]
        log_per_car.edge_travel_info.append(new_edge_log)
        log_per_car.travel_time += new_edge_log.travel_time
        log_per_car.wait_time += new_edge_log.wait_time


class _LogStorage:
    def __init__(self, id_):
        """"""
        self.travel_time = 0
        self.wait_time = 0
        self.edge_travel_info = []
        self.id_ = id_

    def export(self):
        """
        Export log as pandas data frame.
        """
        edge_travel_logs = list(map(lambda x: x.export(), self.edge_travel_info))
        return pd.DataFrame({
            'travel_time': [self.travel_time],
            'wait_time': [self.wait_time],
            'edge_travel_info': [edge_travel_logs]
        })

    def export_timely(self):
        """"""
        edge_travel_logs = list(map(lambda x: x.export(), self.edge_travel_info))
        start_time = []
        travel_time = []
        wait_time = []
        edge_id = []
        reroute_count = []
        ids = [self.id_] * len(edge_travel_logs)

        for edge_log in edge_travel_logs:
            start_time.append(edge_log[0])
            travel_time.append(edge_log[1])
            wait_time.append(edge_log[2])
            edge_id.append(edge_log[3])
            reroute_count.append(edge_log[4])

        return pd.DataFrame({
            'id': ids,
            'start_time': start_time,
            'travel_time': travel_time,
            'wait_time': wait_time,
            'edge_id': edge_id,
            'reroute_count': reroute_count,
        })



class _EdgeLogStorage:
    def __init__(self, start_time, travel_time, wait_time, edge_id, reroute_count):
        """"""
        self.start_time = start_time
        self.travel_time = travel_time
        self.wait_time = wait_time
        self.edge_id = edge_id
        self.reroute_count = reroute_count

    def export(self):
        """
        Exports as list in the order [travel_time, wait_time, edge_id].
        """
        return [self.start_time, self.travel_time, self.wait_time, self.edge_id, self.reroute_count]


class _AutoStorage:
    def __init__(self, time):
        """"""
        self.start_time = time
        self.last_time_step = time
        self.current_edge_id = None
        self.travel_time = 0
        self.wait_time = 0
        self.reroute_count = 0

    def get_edge_travel_info(self, current_time, edge):
        """
        Checks if the auto has moved to another edge. In this case, returns an `_EdgeLogStorage` instance
        with the data about the travel on the previous edge.

        :param int current_time: Current time in ms
        :param string edge: Edge to perform the check for
        :return boolean: `_EdgeLogStorage` if the auto has moved to another edge, `None` otherwise
        """
        # begin of simulation, go on
        if self.current_edge_id is None:
            self.current_edge_id = edge
            self.last_time_step = current_time
            return None

        elapsed_time = self._get_elapsed_time(current_time)
        self.travel_time += elapsed_time

        # we stepped on a new edge
        if self.current_edge_id != edge:
            new_edge_log = _EdgeLogStorage(self.start_time, self.travel_time, self.wait_time, self.current_edge_id,
                                           self.reroute_count)

            # prepare for new edge trip
            self.start_time = current_time
            self.wait_time = 0
            self.travel_time = 0
            self.current_edge_id = edge
            self.reroute_count = 0

            return new_edge_log

        return None

    def log_stop(self, time_stopped):
        """
        Increments the wait_time for the current edge trip.

        :param int time_stopped: Time stopped in ms
        """
        self.wait_time += time_stopped

    def _get_elapsed_time(self, current_time):
        """
        Returns time in ms since the method was last called.
        NOTE: Also updates the current time.

        :param int current_time: Current time in ms
        """
        elapsed_time = current_time - self.last_time_step
        self.last_time_step = current_time

        return elapsed_time
