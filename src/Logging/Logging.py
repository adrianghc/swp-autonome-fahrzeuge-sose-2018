import os
from abc import ABC, abstractmethod


class Logger(ABC):
    """
    Abstract class for loggers.
    """

    scenario = 'Default'
    enabled = True

    @classmethod
    def set_scenario(cls, scenario):
        """
        Set scenario to define output file (e.g NYSCrash_vehicle_log.scv).

        :param string scenario: The scenario name
        """

        cls.scenario = scenario

    @classmethod
    def disable(cls):
        """
        Disables logging.
        """
        cls.enabled = False

    @classmethod
    def _should_log(cls, instance):
        """
        :param instance: The instance that is being logged
        """
        return cls.enabled

    @classmethod
    def get_log_file(cls):
        """
        Returns the path of the log file.
        """
        file_path = os.path.dirname(__file__)
        return os.path.join(file_path, '..', '..', 'out', '{}_{}.csv'.format(cls.scenario, cls.get_log_type()))

    @classmethod
    @abstractmethod
    def write_logs(cls):
        """
        Call at the end of the simulation. Writes log to file.
        """
        pass

    @staticmethod
    @abstractmethod
    def get_log_type():
        """
        E.g. vehicle_log. Defines log file to be "<scenario><log type>.csv".
        """
        pass
