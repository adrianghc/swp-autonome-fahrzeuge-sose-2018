import numpy as np
import math


def get_distance_between_positions(a, b):
    """
    Returns the Euclidean distance between two points.
    """
    assert (len(a) == len(b)), 'Cannot find Euclidean distance between points of different dimensions!'
    assert type(a).__module__ == np.__name__, 'Expected numpy array'
    assert type(b).__module__ == np.__name__, 'Expected numpy array'

    # seems to be the most efficient way to compute Euclidean distance
    # further, we can precompute the squares
    # used in sklearn ->
    # http://scikit-learn.org/stable/modules/generated/sklearn.metrics.pairwise.euclidean_distances.html
    return np.sqrt(np.abs(a.dot(a) - 2 * a.dot(b) + b.dot(b)))


def get_distance_between_positions_precomputed(a_sq, b_sq, a, b):
    """
    Returns the Euclidean distance between two points, assuming they are squared.
    """

    assert (len(a) == len(b)), 'Cannot find Euclidean distance between points of different dimensions!'
    assert type(a).__module__ == np.__name__, 'Expected numpy array'
    assert type(b).__module__ == np.__name__, 'Expected numpy array'

    return np.sqrt(np.abs(a_sq - 2 * np.dot(a, b) + b_sq))


def minutes_to_milliseconds(minutes):
    """
    Converts minutes to milliseconds.
    """
    return math.ceil(minutes * 60 * 1000)


def remove_none_keys_from_dict(dict_):
    """
    Remove entries from the given dictionary where the key is `None`.
    """
    for key in list(dict_.keys()):
        if dict_[key] is None:
            del dict_[key]

    return dict_


def get_reversed_edge(edge_id):
    """
    Returns the edge in the other direction.
    """
    if str.startswith(edge_id, '-'):
        return edge_id[1:]

    return '-' + edge_id
