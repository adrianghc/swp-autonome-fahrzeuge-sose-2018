class SimulationActor:
    """
    An actor taking part in the simulation, e.g. a vehicle or a beacon.
    """

    def __init__(self):
        """"""
        self.time = 0

    def get_time(self):
        """
        Gets the actor's simulation time.

        :return int: Current simulation time in ms
        """
        return self.time

    def set_time(self, time):
        """
        Sets/updates the actor’s simulation time.

        :param time: The new simulation time
        """
        self.time = time

    def on_time_step(self, current_time, elapsed_time):
        """
        Handle to be called from simulation manager on each time step.

        :param int current_time: The current simulation time
        :param int elapsed_time: The elapsed simulation time
        """

        self.time = current_time
