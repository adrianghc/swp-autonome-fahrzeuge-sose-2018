import itertools
import math

from sumolib import net
from traci import vehicle, junction, simulation
from InformationBeacons import BeaconFactory
from InformationSharing.MessageBroker import MessageBroker
from InformationSharing.InformationPeer import InformationPeer
from Vehicles import VehicleFactory
from Logging.AutoLogging import AutoLogger

graph_net = None


class SimulationManager:
    """
    Keeps information about relevant simulation data.
    Manages vehicle and information beacon instances, notifies them on steps.
    Notifies MessageBroker on simulation steps.
    """

    _INFO_BEACON_ID_PREFIX = 'beacon-'

    def __init__(self, net_config):
        """
        :param string net_config: Path to network configuration file
        """

        global graph_net
        graph_net = net.readNet(net_config)
        self.message_broker = MessageBroker()
        self.vehicles = {}
        self.information_beacons = None
        self.latest_time = -1
        self.current_time_in_ms = 0

    def step(self):
        """
        Handle to be called on `traci::simulationStep()` to allow custom scripts to be executed on each step.
        """

        ########################################################################
        # Initialize information beacons. These have static positioning
        # and need no further handling
        ########################################################################

        if self.information_beacons is None:
            self.information_beacons = self._get_information_beacons()

        ########################################################################
        # Handle simulation timing
        ########################################################################

        self.current_time_in_ms = math.ceil(simulation.getTime() * 1000)
        elapsed_time = 0

        if self.latest_time != -1:
            elapsed_time = self.current_time_in_ms - self.latest_time

        self.latest_time = self.current_time_in_ms

        ########################################################################
        # Simulate actors and message propagation
        ########################################################################

        self._update_vehicles()
        self._simulate_actors(self.current_time_in_ms, elapsed_time)
        self.message_broker.propagate(self._get_information_peers(), self.current_time_in_ms)

    def _update_vehicles(self):
        """
        Makes sure `self.vehicles` lists **exactly** the vehicles in the simulation from `traci.vehicle::getIDList()`
        """

        vehicles_within_simulation = vehicle.getIDList()

        # remove vehicles that are no longer within the simulation
        for old_id in self.vehicles.keys() - vehicles_within_simulation:
            AutoLogger.on_auto_arrived(old_id)
            del self.vehicles[old_id]

        for new_id in vehicles_within_simulation - self.vehicles.keys():
                new_vehicle = VehicleFactory.create(vehicle.getTypeID(new_id), new_id)
                self.vehicles[new_id] = new_vehicle
                # setting time here as we need it accurate in the logger
                new_vehicle.set_time(self.current_time_in_ms)
                AutoLogger.on_auto_departed(new_vehicle)

    def _simulate_actors(self, current_time, elapsed_time):
        """
        Method called on simulation step to give each `SimulationActor` instance a chance to observe and send messages.

        Essentially just calls `SimulationActor::on_time_step()` for all instances.
        """

        actors_generator = itertools.chain(self.vehicles.values(), self.information_beacons.values())

        for actor in actors_generator:
            actor.on_time_step(current_time, elapsed_time)

    @classmethod
    def _get_information_beacons(cls):
        """
        Returns instances of information beacon and their position.
        """

        # We assume information beacons are stationary, so no further information shall be required
        # For now, only junctions are information beacons, the signature of this method shall not change in
        # other cases however

        information_beacons = {}
        junction_ids = junction.getIDList()
        beacon_junction_ids = list(filter(lambda x: str.startswith(x, SimulationManager._INFO_BEACON_ID_PREFIX),
                                          junction_ids))

        for junction_id in beacon_junction_ids:
            information_beacons[junction_id] = BeaconFactory.create(junction_id, junction.getPosition(junction_id))

        return information_beacons

    def _get_information_peers(self):
        """
        Returns all instances of `InformationPeer` within the simulation.
        """

        # Method created to provide abstraction in case further information peers are added

        autonomous_cars = filter(lambda x: isinstance(x, InformationPeer), self.vehicles.values())
        return list(itertools.chain(autonomous_cars, self.information_beacons.values()))
