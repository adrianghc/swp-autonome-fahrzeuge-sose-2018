from traci import vehicle
from Vehicles.Vehicle import Vehicle

################################################################################
# Class for the behavior of cars which are going to create an accident
################################################################################


class CrazyCar(Vehicle):
    def __init__(self, id_):
        super().__init__(id_)
        self.route = vehicle.getRoute(self.id_)
        vehicle.setStop(self.id_, self.route[-1], duration=9999999999999)

    def observe(self, elapsed_time):
        super().observe(elapsed_time)

        current_edge = vehicle.getRoadID(self.id_)
        if current_edge == self.route[-1]:
            vehicle.setStop(self.id_, current_edge, duration=9999999999999)

        # TODO: add further logic here if required
