from Simulation.SimulationActor import SimulationActor


class Vehicle(SimulationActor):
    """
    Base class for all vehicles (e.g. Truck, GoingToCrashVehicle etc.) which are needed for the simulation.
    Defines contract between `Simulation` and vehicle instances.
    Every vehicle within the simulation should receive an instance of this class, so that we can save data specific for
    this particular vehicle within.
    """

    def __init__(self, id_):
        """
        :param id_: The id of traci.vehicle
        """

        super().__init__()
        self.id_ = id_

    def observe(self, elapsed_time):
        """
        Handle called from `SimulationManager` to give cars a chance to observe situation and send messages
        on each simulation step.

        :param int elapsed_time: Time elapsed since last call
        """

        pass

    ############################################################################
    # Implement interface from SimulationActor
    ############################################################################

    def on_time_step(self, current_time, elapsed_time):
        super().on_time_step(current_time, elapsed_time)
        self.observe(elapsed_time)
