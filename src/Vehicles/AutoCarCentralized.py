import numpy as np
import helpers
from random import randint
from Vehicles.AutoCar import AutoCar
from Vehicles.Vehicle import Vehicle
from InformationSharing.Message import Message, MessageTopic
from InformationSharing.MessageScheduling import MessageScheduler
from InformationKnowledge.InformationStorage import InformationStorage
from Logging.CentralizedMessageLogging import CentralizedMessageLogger

################################################################################
# Class to fake centralized communication with peer-to-peer with infinity
# transmission range. We are depending on information beacons to send
# information to new vehicles. Message hopping is disabled. Message propagation
# still works to manage knowledge store, but messages are never sent
################################################################################


class AutoCarCentralized(AutoCar):
    """
    Autonomous crazy cars.
    """
    POSITION_UPDATE_INTERVAL = helpers.minutes_to_milliseconds(.5)  # each 30 seconds

    def __init__(self, id_):
        super().__init__(id_)
        # check message relevance each 15-20 seconds
        self._message_scheduler = MessageScheduler(min_wait=15000, max_wait=20000, initial_max_wait=20000)
        # and fix reference to scheduler in  information storage
        self._knowledge_store = InformationStorage(message_scheduler=self._message_scheduler,
                                                   on_new_information_cb=self.on_new_information)

        # flag for handling notifying others vs being notified
        self._did_just_send = False
        # decide a random "location update" state (see AutoCarCentralized::_handle_location_updates())
        self._time_to_position_update = randint(0, self.POSITION_UPDATE_INTERVAL)

    def send_message(self, message):
        super().send_message(message)

        # set flag for the use in AutoCarCentralized::on_new_information()
        self._did_just_send = True

    def on_new_information(self, topic, information):
        super().on_new_information(topic, information)

        # handle logging of centralized communication

        if self._did_just_send:
            # we just observer something and notified others, therefore log to_server here
            # CentralizedMessageLogger.log_message_to_server(self.get_time())

            # NOTE: the logging was handled at the beacons, to prevent 2 vehicles from
            # observing something at the same time and both notifying, as this is an edge case
            # and should not be covered in the statistical analysis. We are trying to be as critical as
            # possible, so it's better to count with as little messages as possible for the centralized case
            self._did_just_send = False
        else:
            # we just received a push-notification from the server
            CentralizedMessageLogger.log_message_from_server(self.get_time())

    def get_transmission_range(self):
        return np.inf

    def _handle_message_hopping(self, message):
        # don't hop messages in centralized case
        pass

    def on_message(self, message):
        super().on_message(message)

        # we are doing this on send usually, after hopping
        if message.topic in self.get_topics_allowed_for_propagation():
            self._message_scheduler.add_to_scheduling(message)

    # NOTE: message propagating is still happening. Though, most of the time all vehicles should

    def observe(self, elapsed_time):
        Vehicle.observe(self, elapsed_time)

        if self._reroute_failed:
            self._reroute_failed = False
            self.handle_rerouting()

        anomaly = self._anomaly_detector.detect_anomaly(elapsed_time)
        if anomaly:
            # Some example of how to use knowledge store to notify others and yourself about a situation
            # if detector.has_anomaly():

            if self._knowledge_store.is_new(MessageTopic.ROAD_CONDITIONS, anomaly):
                msg = Message(MessageTopic.ROAD_CONDITIONS, anomaly)
                self.send_message(msg)

        # NOTE: here we miss adding a callback for send_message
        self._message_scheduler.handle_message_propagation(elapsed_time, is_relevant_cb=self.is_message_relevant)

        self._handle_location_updates(elapsed_time)

    def _handle_location_updates(self, elapsed_time):
        """
        Fake centralized system in which each vehicle sends a location update to the server
        in some time interval.

        :param int elapsed_time: Elapsed time in ms, simulation time passed since last call
        """

        self._time_to_position_update -= elapsed_time

        if self._time_to_position_update <= 0:
            self._time_to_position_update = self.POSITION_UPDATE_INTERVAL
            CentralizedMessageLogger.log_message_to_server(self.get_time())
