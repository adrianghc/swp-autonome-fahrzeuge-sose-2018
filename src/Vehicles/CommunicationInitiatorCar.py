from traci import vehicle

from Vehicles.AutoCar import AutoCar
from InformationSharing.Message import Message, MessageTopic
from InformationKnowledge.Information import AccidentInformation

################################################################################
# Class for the behavior of a car which will transmit a message
# when it enters the simulation. Used for NYSInfoSharingDemo scenario
################################################################################


class CommunicationInitiatorCar(AutoCar):
    """
    When entering the simulation, instances of this class will send a debug message. Otherwise
    handle the same way `AutoCar` instances do.
    """

    def __init__(self, id_):
        super().__init__(id_)
        vehicle.setColor(self.id_, (0, 255, 0))
        self.initial_message_sent = False

    def observe(self, elapsed_time):
        super().observe(elapsed_time)

        if not self.initial_message_sent:
            msg = Message(MessageTopic.INFORMATION_SHARING_DEMO, AccidentInformation())
            self.send_message(msg)
            self.initial_message_sent = True
