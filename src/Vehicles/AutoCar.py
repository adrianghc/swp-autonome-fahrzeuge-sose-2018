import numpy as np

from traci import vehicle
from traci.exceptions import TraCIException
from InformationSharing.Message import Message, MessageTopic
from Routing.DefaultRouter import DefaultRouter
from Routing.DijkstraShortestPath import DijkstraShortestPath
from Vehicles.Vehicle import Vehicle
from Detection.DefaultTrafficDetector import DefaultTrafficDetector
from InformationSharing.MessageScheduling import MessageScheduler
from InformationSharing.InformationPeer import InformationPeer
from InformationKnowledge.InformationStorage import InformationStorage
from Logging.AutoLogging import AutoLogger


################################################################################
# Class for the behavior of default vehicles
# Here the "mass" behavior is meant, not the cars that are e.g. going  to crash
################################################################################


class AutoCar(Vehicle, InformationPeer):
    """
    Autonomous car class. By default shares information with other autos about information it knows
    and detects road conditions worthy of notifying.
    """

    _REROUTE_RETRY_COUNT = 2

    def __init__(self, id_):
        """"""
        Vehicle.__init__(self, id_)
        InformationPeer.__init__(self)

        self.route = vehicle.getRoute(self.id_)
        self._reroute_failed = False
        self._reroute_retries_left = self._REROUTE_RETRY_COUNT

        # caches need for get_position and get_squared_pos
        self._edge_cache = {'at_time': -1}
        self._position_cache = {'position': [0, 0], 'at_time': -1}
        self._sq_pos_cache = {'position': 0, 'at_time': -1}

        self._message_scheduler = MessageScheduler()
        self._knowledge_store = InformationStorage(message_scheduler=self._message_scheduler,
                                                   on_new_information_cb=self.on_new_information)
        self._anomaly_detector = DefaultTrafficDetector(self)
        self._router = DefaultRouter(self.id_, DijkstraShortestPath)

    def on_new_information(self, topic, information):
        """
        Called when there is new information.

        :param topic: Topic the information object corresponds to
        :param information: The `IInformation` object containing the new information
        """

        # vehicle.setColor(self.id_, (255, 0, 0))
        if topic == MessageTopic.ROAD_CONDITIONS:
            self.handle_rerouting()

    def handle_rerouting(self):
        """
        Attempts to recalculate route and handles possible errors.
        """

        current_edge_index = self.get_edge_index()
        issue_objects = self._knowledge_store.get_info_for_topic(MessageTopic.ROAD_CONDITIONS)

        try:
            rest_route = self.route[current_edge_index:]

            # reroute only if we are not on the last edge of our route
            if len(rest_route) >= 2:
                self._router.reroute(issue_objects, rest_route)
        except TraCIException:  # reroute impossible (e.g. auto already started turning right but should go straight
            self._reroute_failed = True
        finally:
            if not self._reroute_failed:
                AutoLogger.on_reroute(self)
                self.route = vehicle.getRoute(self.id_)

    ############################################################################
    # Implement interface from Vehicle
    ############################################################################

    def observe(self, elapsed_time):
        super().observe(elapsed_time)

        ############################################################################
        # Handle rerouting in case it failed
        ############################################################################

        if self._reroute_failed:
            self._reroute_failed = False

            if self._reroute_retries_left > 0:
                self.handle_rerouting()
                self._reroute_retries_left -= 1
            else:
                self._reroute_retries_left = self._REROUTE_RETRY_COUNT


        ############################################################################
        # Detect obstacles and notify others if the information is not already
        # known
        ############################################################################

        anomaly = self._anomaly_detector.detect_anomaly(elapsed_time)
        if anomaly:
            # Some example of how to use knowledge store to notify others and yourself about a situation
            # if detector.has_anomaly():

            if self._knowledge_store.is_new(MessageTopic.ROAD_CONDITIONS, anomaly):
                msg = Message(MessageTopic.ROAD_CONDITIONS, anomaly)
                self.send_message(msg)
        self._message_scheduler.handle_message_propagation(elapsed_time, self.send_message, self.is_message_relevant)

    ############################################################################
    # Implement interface from InformationPeer
    ############################################################################

    def send_message(self, message):
        super().send_message(message)

        if message.topic in self.get_topics_allowed_for_propagation():
            self._message_scheduler.add_to_scheduling(message)

    def on_message(self, message):
        super().on_message(message)
        self._message_scheduler.handle_conflicts(message)

    def get_id(self):
        return self.id_

    def get_messages_propagating(self):
        return self._message_scheduler.get_all_scheduled_messages()

    def get_position(self):
        # Calls to traci are extremely slow. Therefore, as this function is very often used, cache it

        # we track position each second for performance reasons...
        current_time = round(self.get_time() / 1000)

        if self._position_cache['at_time'] == current_time:
            return self._position_cache['position']

        self._position_cache['at_time'] = current_time
        pos = np.array(vehicle.getPosition(self.id_))
        self._position_cache['position'] = pos

        return pos

    def get_squared_pos(self):
        # same caching logic as above

        current_time = round(self.get_time() / 1000)

        if self._sq_pos_cache['at_time'] == current_time:
            return self._sq_pos_cache['position']

        self._sq_pos_cache['at_time'] = current_time
        pos = np.array(self.get_position())
        sq_pos = pos.dot(pos)
        self._sq_pos_cache['position'] = sq_pos

        return sq_pos

    def get_transmission_range(self):
        return 50

    def get_topics_allowed_for_propagation(self):
        return [MessageTopic.ROAD_CONDITIONS, MessageTopic.INFORMATION_SHARING_DEMO]

    def get_topics_subscribed(self):
        return [MessageTopic.ROAD_CONDITIONS, MessageTopic.INFORMATION_SHARING_DEMO]

    ############################################################################

    def get_edge_index(self):
        """
        Returns the index of the current edge in the route.
        """
        # TODO: docstring docu

        return vehicle.getRouteIndex(self.id_)
