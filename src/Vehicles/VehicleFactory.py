from enum import Enum
from Vehicles.AutoCar import AutoCar
from Vehicles.AutoCarCentralized import AutoCarCentralized
from Vehicles.CrazyCar import CrazyCar
from Vehicles.AutoCrazyCar import AutoCrazyCar
from Vehicles.CommunicationInitiatorCar import CommunicationInitiatorCar
from Vehicles.AutoCarColorDemo import AutoCarColorDemo


def create(type_, id_):
    """
    Creates a suited vehicle instance based on the given type using `VehicleType` enum to decide.
    Adjust this method to add further vehicle classes.

    :param string type_: vehicle type from traci.vehicle::getTypeID()
    :param string id_: id from traci.vehicle::getIDList()
    :return:
    """

    if type_ == VehicleType.AUTO_CAR.value:
        return AutoCar(id_)
    elif type_ == VehicleType.AUTO_CAR_COLOR_DEMO.value:
        return AutoCarColorDemo(id_)
    elif type_ == VehicleType.CRAZY_CAR.value:
        return CrazyCar(id_)
    elif type_ == VehicleType.AUTO_CRAZY_CAR.value:
        return AutoCrazyCar(id_)
    elif type_ == VehicleType.COMMUNICATION_INITIATOR_CAR.value:
        return CommunicationInitiatorCar(id_)
    elif type_ == VehicleType.AUTO_CAR_CENTRALIZED.value:
        return AutoCarCentralized(id_)
    else:  # default case
        return AutoCar(id_)


class VehicleType(Enum):
    AUTO_CAR = 'auto_car'
    AUTO_CAR_CENTRALIZED = 'auto_car_centralized'
    AUTO_CAR_COLOR_DEMO = 'auto_car_color_demo'
    CRAZY_CAR = 'crazy_car'
    AUTO_CRAZY_CAR = 'auto_crazy_car'
    COMMUNICATION_INITIATOR_CAR = 'communication_initiator_car'
