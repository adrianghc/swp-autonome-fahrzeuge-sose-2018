from Vehicles.AutoCar import AutoCar
from Vehicles.CrazyCar import CrazyCar

################################################################################
# Class for the behavior of cars which are going to create an accident
# AND are autonomous cars
################################################################################


class AutoCrazyCar(CrazyCar, AutoCar):
    """
    Autonomous crazy cars.
    """
    def __init__(self, id_):
        AutoCar.__init__(self, id_)
        CrazyCar.__init__(self, id_)

    def on_new_information(self, topic, information):
        pass
