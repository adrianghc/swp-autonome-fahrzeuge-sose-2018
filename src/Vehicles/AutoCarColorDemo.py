from traci import vehicle

from Vehicles.AutoCar import AutoCar

################################################################################
# Class for the behavior of autonomous cars within the
# NYSInfoSharingDemo scenario
################################################################################


class AutoCarColorDemo(AutoCar):
    """
    Instances of this class behave as autonomous cars, just receive a red color when they
    are transmitting messages
    """
    TRANSMITTING_COLOR = (255, 0, 0)

    def __init__(self, id_):
        super().__init__(id_)
        self.is_transmitting = False
        self.default_color = vehicle.getColor(self.id_)

    def observe(self, elapsed_time):
        super().observe(elapsed_time)

        is_transmitting = len(self._message_scheduler._schedule_packet_store.get_scheduled_messages()) > 0
        if self.is_transmitting != is_transmitting:
            self.is_transmitting = is_transmitting
            vehicle.setColor(self.id_, AutoCarColorDemo.TRANSMITTING_COLOR if is_transmitting else self.default_color)

