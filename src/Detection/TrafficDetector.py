from abc import ABC, abstractmethod


class TrafficDetector(ABC):
    def __init__(self, vehicle):
        """
        :param Vehicle vehicle: An instance of `Vehicle` for which to perform traffic detection
        """
        self.vehicle = vehicle

    def detect_anomaly(self):
        """
        Runs the traffic anomaly detection algorithm.

        :return boolean: True if a traffic anomaly was detected
        """
