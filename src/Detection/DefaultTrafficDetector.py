import helpers
from traci import vehicle, lane
from InformationKnowledge.Information import AccidentInformation
from Simulation import SimulationManagement
from Detection.TrafficDetector import TrafficDetector
from Logging.AutoLogging import AutoLogger


class DefaultTrafficDetector(TrafficDetector):

    def __init__(self, vehicle, tstoplim=helpers.minutes_to_milliseconds(2), speedfac=0.25):
        """
        Default implementation of `TrafficDetector`.

        :param Vehicle vehicle: An instance of `Vehicle` for which to perform traffic detection
        :param double tstoplim: Maximum stopping time that is not considered anomalous
        :param double speedfac: Factor between 0 and 1 setting at what ratio of the maximum permitted speed of a road
            the cumulative vehicle speed is considered anomalous
        """
        super().__init__(vehicle)
        self.speed_estimator = _AverageSpeedEstimator()
        self.time_started_stopping = -1
        self.TIME_STOP_LIMIT = tstoplim
        self.MAX_SPEED_FAC = speedfac

    def detect_anomaly(self, elapsed_time=1000):
        """
        Checks if stopping time and speed estimator of a vehicle are anomalous.
        Indicators for traffic anomalies are:
        1. Long waiting times at green traffic lights
        2. Average speeds considerably below the max speed of the lane the vehicle is currently on.

        :return AccidentInformation: Returns `AccidentInformation` object if anomaly is detected
        """
        net = SimulationManagement.graph_net

        curr_speed = vehicle.getSpeed(self.vehicle.id_)
        curr_edge = vehicle.getRoadID(self.vehicle.id_)
        is_internal_edge = str.startswith(curr_edge, ':')
        curr_lane = vehicle.getLaneID(self.vehicle.id_)
        curr_time = self.vehicle.get_time()

        # We don't detect traffic jams on internal edges, which are part of junctions.
        if is_internal_edge:
            return None

        AutoLogger.on_step(self.vehicle, curr_edge)

        if curr_speed == 0:
            if self.time_started_stopping == -1:
                self.time_started_stopping = curr_time
            time_stopped = curr_time - self.time_started_stopping
        else:
            if self.time_started_stopping != -1:
                AutoLogger.on_stop_finished(self.vehicle, curr_time - self.time_started_stopping)
            self.time_started_stopping = -1
            time_stopped = 0

        next_traffic_lights = vehicle.getNextTLS(self.vehicle.id_)
        tl_distance = 0
        state_of_next_tl = None
        if next_traffic_lights:
            tl_distance = next_traffic_lights[0][2]
            state_of_next_tl = next_traffic_lights[0][3]

        tls_allows = (state_of_next_tl in ["G", "g"] or state_of_next_tl is None) or tl_distance > 10
        acc_speed, is_valid = self.speed_estimator.update(curr_speed, elapsed_time) if tls_allows else (0, False)
        lane_max_speed = lane.getMaxSpeed(curr_lane)
        speed_too_slow = acc_speed < self.MAX_SPEED_FAC * lane_max_speed and is_valid
        wait_too_long = time_stopped > self.TIME_STOP_LIMIT
        decelerating = acc_speed > curr_speed
        obstacle_detected = tls_allows and ((speed_too_slow and decelerating) or wait_too_long)

        if obstacle_detected:
            self.speed_estimator.estimated = lane_max_speed
            vehicle.setColor(self.vehicle.id_, (0, 0, 255))
            return AccidentInformation(self.vehicle.get_position(), curr_edge, curr_lane, acc_speed)
        return None


class _AverageSpeedEstimator:
    def __init__(self, estimation_period=helpers.minutes_to_milliseconds(3)):
        """
        :param double cache_weight: Value within [0,1] specifying the weight of the cumulative average
        :param gauge: Time gauge, by default set to 1000ms
        """
        self.estimated = 0
        self._time_passed_since_beginning = 0
        self._estimation_period = estimation_period

    def update(self, new_speed, elapsed_time):
        """
        Updates the speed estimator.

        :param new_speed: Current speed at the time of call
        :param elapsed_time: Time elapsed since the last update
        :return double: The estimated speed, and whether the estimation is considered valid
        """
        if elapsed_time == 0:
            return 0, False

        self._time_passed_since_beginning += elapsed_time
        fitting_in_interval = self._estimation_period / elapsed_time

        self.estimated -= self.estimated / fitting_in_interval
        self.estimated += new_speed / fitting_in_interval

        return self.estimated, self._is_estimation_valid()

    def _is_estimation_valid(self):
        """
        An estimation is considered valid, if it was done over at least
        a `self._minimum_estimation_period` time (30 seconds as of 20.09.2018).

        :return boolean: Whether the estimation is considered valid.
        """

        return self._time_passed_since_beginning > self._estimation_period

class _SpeedEstimator:
    def __init__(self, cache_weight=.9, gauge=1000):
        """
        :param double cache_weight: Value in [0,1] specifying the weight of the cumulative average
        :param gauge: Time gauge, by default set to 1000ms
        """
        self.estimated = 0
        self._cache_weight = cache_weight
        self.gauge = gauge

    def update(self, new_speed, elapsed_time):
        """
        Updates the speed estimator.

        :param new_speed: Current speed at the time of call
        :param elapsed_time: Time elapsed since the last update

        :return double: The estimated speed, and whether the estimation is considered valid
        """
        alpha = (1 - self._cache_weight) * elapsed_time / self.gauge
        self.estimated = self.estimated * (1 - alpha) + new_speed * alpha

        return self.estimated
