import numpy as np

from traci import poi
from InformationSharing.InformationPeer import InformationPeer
from InformationSharing.Message import MessageTopic
from InformationSharing.MessageScheduling import MessageScheduler
from Simulation.SimulationActor import SimulationActor


class InformationBeacon(SimulationActor, InformationPeer):
    """
    Acts as a repeater for nearby messages.
    Information beacon placed on a stationary position which broadcasts relevant messages without caring
    about conflicts and who is broadcasting at the moment.
    """

    DEFAULT_COLOR = (160, 160, 160, 255)  # gray in RGB
    TRANSMITTING_COLOR = (255, 0, 0, 255)  # red in RGB

    def __init__(self, id_, position):
        SimulationActor.__init__(self)
        InformationPeer.__init__(self)
        self.id_ = id_
        self.position = np.array(position)
        self.sq_position = self.position.dot(self.position)
        self._message_scheduler = MessageScheduler(min_wait=1000, initial_max_wait=1000)

        self.gui_poi_id = str(id(self))
        self.add_gui_mark()

    def add_gui_mark(self):
        """
        Adds a marker to the beacon inside the SUMO GUI.
        """
        poi.add(self.gui_poi_id, self.position[0], self.position[1], InformationBeacon.DEFAULT_COLOR,
                layer=2)

    ############################################################################
    # Implement interface from SimulationActor
    ############################################################################

    def on_time_step(self, current_time, elapsed_time):
        super().on_time_step(current_time, elapsed_time)
        self._message_scheduler.handle_message_propagation(elapsed_time, is_relevant_cb=self.is_message_relevant,
                                                           send_message_cb=self.send_message)

    ############################################################################
    # Implement interface from InformationPeer
    ############################################################################

    def get_topics_allowed_for_propagation(self):
        return [MessageTopic.ROAD_CONDITIONS, MessageTopic.INFORMATION_SHARING_DEMO]

    def get_topics_subscribed(self):
        return [MessageTopic.ROAD_CONDITIONS, MessageTopic.INFORMATION_SHARING_DEMO]

    def on_message(self, message):
        super().on_message(message)

        if message.topic in self.get_topics_allowed_for_propagation():
            self._message_scheduler.add_to_scheduling(message)

    def get_messages_propagating(self):
        return self._message_scheduler.get_all_scheduled_messages()

    def get_id(self):
        return self.id_

    def get_position(self):
        return self.position

    def get_squared_pos(self):
        return self.sq_position

    def get_transmission_range(self):
        return 200
