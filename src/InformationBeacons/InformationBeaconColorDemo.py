from traci import poi
from InformationBeacons.InformationBeacon import InformationBeacon


class InformationBeaconColorDemo(InformationBeacon):
    """
    Information beacon for demonstration purposes. Acts like a normal information beacon, but receives
    a red color in case it is transmitting messages.
    """
    def __init__(self, id_, position):
        super().__init__(id_, position)
        self.is_transmitting = False

    def toggle_color(self):
        """
        Set color of PoI point on map according to whether the beacon is transmitting messages at the moment
        (uses self.is_transmitting and self._gui_poi_id)
        """
        color = InformationBeacon.TRANSMITTING_COLOR if self.is_transmitting else InformationBeacon.DEFAULT_COLOR
        poi.setColor(self.gui_poi_id, color)

    ############################################################################
    # Implement interface from SimulationActor
    ############################################################################

    def on_time_step(self, current_time, elapsed_time):
        super().on_time_step(current_time, elapsed_time)

        # NOTE: we are using private functionality here and breaking the information hiding principle.
        #       This is done purely to show the information sharing functionality. Usually, one must not
        #       care if one transmits a message or not, therefore the functionality is hidden. The demonstration
        #       case is another story though...

        is_transmitting = len(self._message_scheduler._schedule_packet_store.get_scheduled_messages())
        if is_transmitting != self.is_transmitting:
            self.is_transmitting = is_transmitting
            self.toggle_color()
