from InformationBeacons.InformationBeacon import InformationBeacon
from InformationBeacons.InformationBeaconColorDemo import InformationBeaconColorDemo
from InformationBeacons.InformationBeaconCentralized import InformationBeaconCentralized


def create(id_, position):
    """
    Factory to create different information beacons. For now used only to
    distinguish between the demo case and the general beacons.
    """
    if str.startswith(id_, 'beacon-color-'):
        return InformationBeaconColorDemo(id_, position)
    elif str.startswith(id_, 'beacon-centralized-'):
        return InformationBeaconCentralized(id_, position)
    elif str.startswith(id_, 'beacon-'):
        return InformationBeacon(id_, position)
    else:
        return InformationBeacon(id_, position)
