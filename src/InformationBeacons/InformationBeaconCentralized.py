import numpy as np
from InformationBeacons.InformationBeacon import InformationBeacon
from Logging.CentralizedMessageLogging import CentralizedMessageLogger


class InformationBeaconCentralized(InformationBeacon):
    """
    Beacon with infinitely large range. Also acts as a server in a centralized communication simulation.
    """

    def __init__(self, id_, position):
        super().__init__(id_, position)

        # Remember, a beacon does not observe, so new information means somebody notified it.
        self._message_scheduler.add_on_insert_callback(self.on_new_information)

    def on_new_information(self, *args):
        """
        Log message being sent to the server.

        :param args: We don't care about the arguments for this method, only the callback functionality is important
        """
        CentralizedMessageLogger.log_message_to_server(self.get_time())

    def get_transmission_range(self):
        """
        Returns the beacon's transmission range.
        """
        return np.inf
