#!/usr/bin/env python

import traci
import time
import argparse
import os
import math

from Simulation.SimulationManagement import SimulationManager
from Logging.AutoLogging import AutoLogger
from Logging.MessageLogging import MessageLogger
from Logging.CentralizedMessageLogging import CentralizedMessageLogger

# NOTE: if you fiddle with those (e.g. set the `SIMULATION_SPEEDUP` to 1 for real time),
# make sure the `MILLISECONDS_PER_SIMULATION_STEP` are enough to ensure a decent frame rate

MILLISECONDS_PER_SIMULATION_STEP = 300
SIMULATION_STEP_LENGTH = 0.001 * MILLISECONDS_PER_SIMULATION_STEP

SIMULATION_SPEEDUP = 100
REAL_TIME_SIMULATION_DELAY = math.ceil(SIMULATION_STEP_LENGTH / SIMULATION_SPEEDUP)


def main(args):
    ############################################################################
    # Get path to configuration files
    ############################################################################

    file_path = os.path.dirname(__file__)
    sumo_config = os.path.join(file_path, '..', 'res', 'scenarios', args.scenario, '{}.sumocfg'.format(args.scenario))
    net_config = os.path.join(file_path, '..', 'res', 'scenarios', args.scenario, '{}.net.xml'.format(args.scenario))

    ############################################################################
    # Sumo commands
    ############################################################################
    sumo_binary = "sumo-gui"
    sumo_cmd = [sumo_binary, "-c", sumo_config, "--step-length", str(SIMULATION_STEP_LENGTH)]

    ############################################################################
    # Simulation
    ############################################################################

    loggers = [AutoLogger, MessageLogger, CentralizedMessageLogger]
    for logger in loggers:

        if args.log:
            logger.set_scenario(args.scenario)
        else:
            logger.disable()

    simulation_manager = SimulationManager(net_config)
    traci.start(sumo_cmd)  # Start TraCI simulation
    print('Simulation started!')
    while (not args.time) or (simulation_manager.latest_time < args.time):
        try:
            traci.simulationStep()
        except traci.FatalTraCIError as e:
            break

        started_time = time.time()
        simulation_manager.step()
        ms_passed = math.ceil((time.time() - started_time)*1000)
        time.sleep(max(0, REAL_TIME_SIMULATION_DELAY - ms_passed))

    if args.log:
        for logger in loggers:
            logger.write_logs()
    print('Simulation finished!')


def parse_args():
    parser = argparse.ArgumentParser(description="Something")
    parser.add_argument('-s', '--scenario', help="define the scenario folder to be used (e.g. \"NewYorkSmall\")",
                        default="NewYorkSmall", required=False)
    parser.add_argument('-l', '--log', help="save log files for vehicles and messages",
                        default=False, action='store_true')
    parser.add_argument('-t', '--time', help="specify time in ms for the simulation to run", type=int, default=False)

    return parser.parse_args()


if __name__ == '__main__':
    main(parse_args())
