import helpers
from enum import Enum


class Message:
    """
    Message "in the air" sent from a car or a beacon, to be distributed to cars and beacons nearby.
    """

    def __init__(self, topic, data):
        """
        :param MessageTopic topic: similar to observer pattern, each message has a topic,
                                    peers can be subscribed to various topics
        :param InformationKnowledge.Information.IInformation data: message data,
            e.g. data for accident saved as key value pairs;
            type chosen to allow various messages
        """
        self.id_ = id(data)
        self.topic = topic
        self.data = data

        # Metadata
        self.metadata = MetaData()

        self._set_default_lifecycle()

    def set_meta(self, time, location, location_sq, sender):
        """
        Sets meta data which is relevant for `MessageBroker`.

        :param int time: the time in milliseconds from the beginning of the simulation; InformationPeer instances
                         have get_time() method for easier use
        :param list location: the location of the message "in the air", used to determine if
                              information peers can receive it
        :param float location_sq: dot product of location with itself for performance reasons
        :param InformationPeer sender: reference to the InformationPeer instance which sent the message
        """
        # Meta data which must never be set again
        if self.metadata.original:
            self.metadata.timestamp = time
            self.metadata.initial_location = location
            self.metadata.initial_location_sq = location_sq
            self.metadata.original = False

        self.metadata.location = location
        self.metadata.location_sq = location_sq
        self.metadata.sender = sender

    def set_lifecycle(self, range_, lifetime):
        """
        :param double range_: The range in meters within which a message shall live
        :param int lifetime: The timespan in milliseconds within which a message shall live
        :return:
        """

        self.metadata.range = range_
        self.metadata.lifetime = lifetime

    def copy(self):
        """
        Creates a copy of the message with new location at propagation.
        """

        new_message = Message(self.topic, self.data)
        new_message.metadata = self.metadata.copy_altered()

        return new_message

    def _set_default_lifecycle(self):
        """"""
        hour_life = helpers.minutes_to_milliseconds(60)
        five_minute_life = helpers.minutes_to_milliseconds(5)

        three_km_distance = 3000
        one_km_distance = 1000

        if self.topic == MessageTopic.ROAD_CONDITIONS:
            self.set_lifecycle(three_km_distance, hour_life)
        elif self.topic == MessageTopic.INFORMATION_SHARING_DEMO:
            self.set_lifecycle(one_km_distance, five_minute_life)
        else:  # default
            self.set_lifecycle(three_km_distance, hour_life)


class MetaData:
    def __init__(self, location=None, location_sq=None, initial_location=None, initial_location_sq=None,
                 range_=0, lifetime=0, timestamp=None, sender=None, original=True):
        """
        :param list location: Location of the message "in the air"
        :param list initial_location: The original location of the first message (in case copied)
        :param int range_: How far in meters the message is still relevant
        :param int lifetime: How long in milliseconds the message is still relevant
        :param int timestamp: The time in which the original message was created
        :param InformationSharing.InformationPeer.InformationPeer sender: The sender of the current message
                                                                          (not original)
        :param bool original: is this message created by the sender
        """

        self.location = location
        self.location_sq = location_sq
        self.initial_location = initial_location
        self.initial_location_sq = initial_location_sq
        self.range = range_
        self.lifetime = lifetime
        self.timestamp = timestamp
        self.sender = sender
        # whether this is the original message (when first created) [True] or copied for redistribution [False]
        self.original = original

    def copy_altered(self, **kwargs):
        """
        Create new `MetaData` instance by using the properties of the one called upon. If an argument is provided
        for some property, it is used for the returned instance.

        Accepts the same arguments as `MetaData::__init__()`.
        """
        altered_copy = MetaData(self.location, self.location_sq, self.initial_location, self.initial_location_sq,
                                self.range, self.lifetime, self.timestamp, self.sender, self.original)

        for arg in kwargs.keys():
            setattr(altered_copy, arg, kwargs[arg])

        return altered_copy


class MessageTopic(Enum):
    ROAD_CONDITIONS = 'ROAD_CONDITIONS'
    INFORMATION_SHARING_DEMO = 'INFORMATION_SHARING_DEMO'
