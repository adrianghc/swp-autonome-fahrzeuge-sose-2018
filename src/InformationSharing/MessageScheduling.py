import helpers
from random import randint


################################################################################
# Template methods for better docstring documentation
################################################################################

def _send_message_template(message):
    """
    Template
    """
    pass


def is_relevant_template(message):
    """
    Template
    """
    return True


def on_insert_cb_template(message):
    """
    Template
    """
    pass


def on_remove_cb_template(message):
    """
    Template
    """
    pass

################################################################################
# END TEMPLATE METHODS
################################################################################


class MessageScheduler:
    """
    Handles message propagation after the initial hopping. It will send the still relevant messages
    automatically in random time steps. Also handles conflicts with other information peers and prevents
    spam when multiple information peers are in close proximity.

    Make sure you call `add_to_scheduling` when sending a message (no matter if it's just a hop or a self-crafted
    message), `handle_conflicts` when receiving a new message and `handle_message_propagation` on simulation steps.

    Uses ALOHA inspired algorithm to decide when to send messages as follows:
     1. Pick a random delay within an initial range.
     2. If the same message has been received within this range, reschedule the message to be sent
        after a random delay (but a twice as big time span is used to pick random delay from).
     3. Repeat 1) and 2) until the message has been sent, then we reschedule it by starting from the initial
        time span to pick random delay from.
    BONUS: A maximum delay (5 seconds) is also defined. This means that the time span to draw random delay from
            stops growing at 5 seconds, delays are still random though.
    """
    def __init__(self, min_wait=None, max_wait=None, initial_max_wait=None):
        """
        :param int min_wait: Lower range of wait time
        :param int max_wait: Upper range of wait time (static)
        :param int initial_max_wait: Upper range of wait time without decay
        """

        self._schedule_packet_store = _ScheduledPacketStore()
        # args for schedule method. remove none keys in order to allow for default values to be used
        self.schedule_kwargs = helpers.remove_none_keys_from_dict(
            {'min_wait': min_wait, 'max_wait': max_wait, 'initial_max_wait': initial_max_wait}
        )
        self._on_remove_callbacks = []
        self._on_insert_callbacks = []

    def add_to_scheduling(self, message):
        """
        Adds a message to the scheduler.
        """
        if self._schedule_packet_store.insert(message):
            scheduled_packet = self._schedule_packet_store.get(message)
            scheduled_packet.schedule(**self.schedule_kwargs)

            self._notify_on_insert(message)

    def handle_conflicts(self, message):
        """
        Handle situations in which another peer sent the same message as a scheduled one.
        :param InformationSharing.Message.Message message: The conflicting message.
        """
        if message in self._schedule_packet_store and self is not message.metadata.sender:
            self._schedule_packet_store.get(message).handle_conflict(**self.schedule_kwargs)

    def handle_message_propagation(self, time_passed,
                                   send_message_cb=_send_message_template, is_relevant_cb=is_relevant_template):
        """
        :param int time_passed: Time since the method was last called
        :param send_message_cb: Callback method of information peer used for sending a message
        :param is_relevant_cb: Callback method of information peer used to check message relevancy
        """

        for _scheduled_packet in self._schedule_packet_store.get_scheduled_messages():
            _scheduled_packet.time_tick(time_passed)

            if _scheduled_packet.wait_time <= 0:
                if is_relevant_cb(_scheduled_packet.message):
                    send_message_cb(_scheduled_packet.message)
                    _scheduled_packet.reschedule(**self.schedule_kwargs)
                else:
                    self._schedule_packet_store.remove(_scheduled_packet.message)
                    self._notify_on_remove(_scheduled_packet.message)

    def get_all_scheduled_messages(self):
        """
        Returns all currently scheduled messages.
        """
        return self._schedule_packet_store.get_scheduled_messages()

    def _notify_on_insert(self, message):
        """
        Notify subscribers when a message was inserted into store.
        """

        for cb in self._on_insert_callbacks:
            cb(message)

    def _notify_on_remove(self, message):
        """
        Notify subscribers when a message was removed from store.
        """
        for cb in self._on_remove_callbacks:
            cb(message)

    ############################################################################
    # Used within InformationStorage
    ############################################################################

    def add_on_remove_callback(self, cb=on_remove_cb_template):
        """
        Add a callback to be called when removing a message from the store.
        """
        self._on_remove_callbacks.append(cb)

    def add_on_insert_callback(self, cb=on_insert_cb_template):
        """
        Add a callback to be called when inserting a message into the store.
        """
        self._on_insert_callbacks.append(cb)


class _ScheduledPacketStore:
    """
    Provides abstraction for storing and accessing messages to be dispatched in the future.
    Packs each stored message in a `_ScheduledPacket`, so that waiting time can be saved and rescheduling can
    be implemented based on number of conflicts (a.k.a. times the message was rescheduled before a dispatch).
    """

    def __init__(self):
        self.store = {}

    def __contains__(self, message):
        """
        Checks if a message exists in the store.

        :param Message.Message message: Message for which to perform the check
        :return boolean: True if the message exists
        """
        return message.id_ in self.store

    def get(self, message):
        """
        Gets a message if it exists in the store.

        :param Message.Message message: The message
        :return Message.Message: The message if it exists, `None` otherwise
        """
        if message in self:
            return self.store[message.id_]

        return None

    def insert(self, message):
        """
        Inserts a message into the store.

        :param Message.Message message: Message to be inserted
        :return boolean: True if non-existing and inserted
        """
        if not (message.id_ in self.store):
            self.store[message.id_] = _ScheduledPacket(message)
            return True

        return False

    def remove(self, message):
        """
        Removes a message from the store.

        :param Message.Message message: Message to be removed
        :return boolean: True if existing and removed
        """
        if message in self:
            del self.store[message.id_]
            return True

        return False

    def get_scheduled_messages(self):
        """
        Returns a copy of all scheduled messages.
        """
        return list(self.store.values())


class _ScheduledPacket:
    MIN_WAIT_TIME = 1000
    INITIAL_MAX_WAIT = 3000
    MAX_WAIT_TIME = 30000

    def __init__(self, message):
        """"""
        self.message = message
        self.conflicts_count = 0
        self.wait_time = 0
        self._send_retries_count = 0

    def reschedule(self, **kwargs):
        """
        Same as schedule, just sets conflicts count to 0.

        :param kwargs: Params of schedule
        """
        self.conflicts_count = 0
        self.schedule(**kwargs)

    def schedule(self, min_wait=MIN_WAIT_TIME, max_wait=MAX_WAIT_TIME, initial_max_wait=INITIAL_MAX_WAIT):
        """
        Schedule message to be sent in the future automatically. The method takes into consideration how
        many conflicts were handled for the message since it was last distributed, messages with more conflicts have
        a higher chance of waiting more.

        The parameters are the same as in the constructor of `MessageScheduler`.

        :param int min_wait: Lower range of wait time
        :param int max_wait: Upper range of wait time (static)
        :param int initial_max_wait: Upper range of wait time without decay
        """
        max_wait_ = min(initial_max_wait * (2 ** self.conflicts_count), max_wait)

        # doubling range for each conflict. However, the range cannot be extended above 5 seconds
        self.wait_time = randint(min_wait, max_wait_)

    def handle_conflict(self, **kwargs):
        """
        Handle situation when another InformationPeer sends the same message as a scheduled one.

        :param kwargs: Params of schedule
        """
        self.conflicts_count += 1
        self.schedule(**kwargs)

    def time_tick(self, time_passed):
        """
        To be called on simulation steps.

        :param time_passed: Time since the `time_tick` method of this scheduled packet was last called
        """
        self.wait_time -= time_passed
