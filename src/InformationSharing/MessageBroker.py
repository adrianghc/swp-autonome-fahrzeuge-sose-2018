import helpers

from collections import deque
from Logging.MessageLogging import MessageLogger


class MessageBroker:
    """
    This class should handle the message propagation between information peers. As we are simulating
    p2p message transfer using Wi-Fi without internet connection
    (or other kind of short range communication), information peers can only send
    messages to other information peers nearby. Those peers however send the messages to further, more distant peers.
    Therefore, a message should propagate until there are no further peers nearby which didn't receive the
    message or the distance to the object of interest within the message (e.g. a car accident on street X) is
    large enough so it's no longer of interest.

    NOTE: MessageBroker was a very fitting name, however we are not really implementing the Message Broker pattern.
          The pattern makes no sense when we are simulating message broadcasts "in the air".

          After all, all peers nearby will receive a message (in reality). Whether they decide to do something
          with it or not is their decision.
    """

    def __init__(self):
        # messages to be sent to nearby information peers
        self._messages_on_fly = deque([])

    def propagate(self, information_peers, simulation_time):
        """
        To be used after a simulation step. After all peers are done sending messages, they should be propagated
        so they are ready for parsing on the next simulation step (e.g. for rerouting).

        :param list information_peers: Peers which are in the current simulation
        """

        # gather messages from information peers
        for peer in information_peers:
            peer_messages = peer.pop_messages_to_send()
            MessageLogger.log_peer(simulation_time, peer, len(peer_messages), 0, 0)
            self._messages_on_fly.extend(peer_messages)

        # propagate until messages stop appearing
        while len(self._messages_on_fly) > 0:
            current_message = self._messages_on_fly.popleft()

            for peer in information_peers:
                if self._is_eligible_for_message(peer, current_message):
                    # send message and add new flying messages in case the peer decided to send them further
                    peer.on_message(current_message)
                    peer_messages = peer.pop_messages_to_send()
                    MessageLogger.extend_peer_log(peer, messages_sent=len(peer_messages), messages_received=1)
                    self._messages_on_fly.extend(peer_messages)

        # log messages distributing on this time step
        for peer in information_peers:
            MessageLogger.extend_peer_log(peer, messages_distributing=len(peer.get_messages_propagating()))

    @classmethod
    def _is_eligible_for_message(cls, peer, message):
        """
        :return boolean: True if the peer is within the range of the message and can therefore receive it
        """

        sender_range = message.metadata.sender.get_transmission_range()
        return helpers.get_distance_between_positions_precomputed(peer.get_squared_pos(), message.metadata.location_sq,
                                                      peer.get_position(), message.metadata.location) < sender_range
