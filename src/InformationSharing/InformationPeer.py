import helpers
from abc import ABC, abstractmethod


class InformationPeer(ABC):
    """
    Abstract class which defines the contract between all information peers (those who send and receive messages)
    and `MessageBroker`.
    """

    def __init__(self):
        self._messages_to_be_send = []
        self._received_message_ids = {}

    def on_message(self, message):
        """
        Handle to be used from `MessageBroker` to send messages to cars.
        """
        self._handle_message_hopping(message)

        # TODO: keep info

    def send_message(self, message):
        """
        Sends a message.
        """
        message.set_meta(self.get_time(), self.get_position(), self.get_squared_pos(), self)

        # As messages are cast "in the air", one will automatically become one's own message.
        # Though this makes little sense, therefore we mark the message as received when we send it.
        self._received_before(message)

        # Save message, the `MessageBroker` will handle the rest.
        self._messages_to_be_send.append(message)

    def pop_messages_to_send(self):
        """
        Clears the list of messages to be sent in addition to sending.

        :return list: The messages which should be sent by the `MessageBroker`
        """
        messages = list(self._messages_to_be_send)
        self._messages_to_be_send.clear()

        return messages

    def is_message_relevant(self, message):
        """
        Return whether the information peer instance finds this message still useful or wants to propagate it further.

        1. Is the auto within the range of relevance of the message?
           (e.g. an accident is relevant only within 3km range)
        2. Is the message still valid w.r.t. time?
           (e.g. a message about traffic conditions older than 2h might not be valid)

        If the message is in the range of relevance and not expired w.r.t. time, it's considered
        relevant for this vehicle.

        :param InformationSharing.Message.Message message: The message for which we want to do the check.
        :return boolean: Whether the message is still considered relevant.
        """
        metadata = message.metadata
        distance_to_original = helpers.get_distance_between_positions_precomputed(
            metadata.initial_location_sq, self.get_squared_pos(), metadata.initial_location, self.get_position())

        # check 1.
        in_range = distance_to_original < metadata.range
        # check 2.
        expired = self.get_time() - metadata.timestamp > metadata.lifetime

        return in_range and not expired

    def get_messages_propagating(self):
        """"""
        return []

    @abstractmethod
    def get_id(self):
        """
        Returns an unique identifier for the information peer.
        """
        pass

    @abstractmethod
    def get_time(self):
        """
        Returns the current time.
        """
        pass

    @abstractmethod
    def get_position(self):
        """
        Returns the current position.
        """
        pass

    @abstractmethod
    def get_squared_pos(self):
        """
        Returns the squared current position.
        """
        pass

    @abstractmethod
    def get_transmission_range(self):
        """
        Returns the range the transmitter of an information peer is capable of delivering messages in.
        """
        pass

    @abstractmethod
    def get_topics_allowed_for_propagation(self):
        """
        Returns topics that the current information peer accepted to propagate
        (cannot force people to propagate random messages).
        """
        pass

    @abstractmethod
    def get_topics_subscribed(self):
        """
        Returns topics of relevance for the current information peer (which one wants to act upon).
        """
        pass

    def _received_before(self, message):
        """
        NOTE: Also adds the message to memory so the method will return True
        next time with the same message

        NOTE: The check is done using message.id_ so coned messages using
        `message::copy_for_distribution()` will return the same result as their original versions

        :param InformationSharing.Message.Message message: Message in question
        :return boolean: True if the message was received before
        """
        received_before = message.id_ in self._received_message_ids
        self._received_message_ids[message.id_] = True
        return received_before

    def _handle_message_hopping(self, message):
        """
        Each car redistributes a received message right away **just once**.
        Afterwards, after n time steps, the car creates a new message and publishes
        it again to notify further cars. This is done to prevent spam between cars.

        :param InformationSharing.Message.Message message: Message in question
        """
        # propagate message to further autos if still relevant, no matter if info is useful for the current car
        if self._should_propagate(message):
            self.send_message(message.copy())

    def _should_propagate(self, message):
        """"""
        return not self._received_before(message) and self.is_message_relevant(message)
