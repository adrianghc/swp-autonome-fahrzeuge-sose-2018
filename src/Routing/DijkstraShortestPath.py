from Routing.ShortestPath import ShortestPath
from math import inf


class DijkstraShortestPath(ShortestPath):
    """
    Calculates the shortest path between two nodes in a network using the Dijkstra algorithm.
    """

    def __init__(self, net_, weights_=None):
        super().__init__(net_, weights_)

        self.distances = {}
        self.precursors = {}
        self.visited = {}
        self.start_node = None
        self.end_node = None
        self.start_edge = None
        self.end_edge = None

    def run(self, start_edge_id, end_edge_id):
        super().run(start_edge_id, end_edge_id)

        self.distances = {}
        self.precursors = {}
        self.visited = {}

        self.start_edge, self.end_edge = self.net.getEdge(start_edge_id), self.net.getEdge(end_edge_id)
        self.start_node, self.end_node = self.start_edge.getToNode(), self.end_edge.getToNode()
        self.precursors[self.start_node] = None, self.start_edge

        self.distances[self.start_node] = 0
        self._add_visited(self.start_node)

        while self.distances:
            closest_node = min(self.distances, key=self.distances.get)

            if closest_node == self.end_node:
                break

            # getFrom() : incoming
            # getTo() : outgoing

            # get outgoing edges by looking at `closest_node` and it's `connection`s (part of sumo network)
            # and picking only those, that the vehicle is allowed to drive two
            # (are outgoing and connected to current edge)
            connections = closest_node.getConnections()
            precursor_node, precursor_edge = self.precursors[closest_node]
            connections_from_precursor = list(filter(lambda x: x.getFrom() == precursor_edge, connections))
            outgoing_edges = list(map(lambda x: x.getTo(), connections_from_precursor))

            for edge in outgoing_edges:
                to_node = edge.getToNode()

                if self._is_visited(to_node):
                    continue

                distance = self.distances[closest_node] + self.weights[edge.getID()]

                if to_node not in self.distances or distance < self.distances[to_node]:
                    self.distances[to_node] = distance
                    self.precursors[to_node] = (closest_node, edge)

            self.distances.__delitem__(closest_node)
            self._add_visited(closest_node)

        return self._get_path_by_traceback(), self.distances[self.end_node] + self.weights[self.start_edge.getID()]

    def _get_path_by_traceback(self):
        """
        Returns the list of edges the shortest path consists of by tracing back the precursors.
        """
        precursor_node, precursor_edge = self.precursors[self.end_node]
        shortest_path = [precursor_edge.getID()]

        while precursor_node is not None:
            precursor_node, precursor_edge = self.precursors[precursor_node]
            shortest_path.append(precursor_edge.getID())

        # remember we are tracing back, so we need the reversed list
        return shortest_path[::-1]

    def _add_visited(self, node):
        self.visited[node.getID()] = True

    def _is_visited(self, node):
        return node.getID() in self.visited
