from abc import ABC, abstractmethod


class Router(ABC):
    def __init__(self, vehicle_id, sp_algorithm):
        """
        Creates a router for the vehicle with the specified id.

        :param string vehicle_id: Sumo id of the vehicle to be routed
        :param ShortestPath sp_algorithm: Shortest path algorithm to use for routing
        """
        super().__init__()
        self.vid = vehicle_id

    @abstractmethod
    def reroute(self, issue_lane, route):
        """
        Reroutes a vehicle with the specified id using the parameters passed.

        :param string issue_lane: Edge around which to calculate a new route
        :param list route: A list of edge ids (route) to be recalculated
        """
        pass
