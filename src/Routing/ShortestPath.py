from abc import ABC, abstractmethod
from sumolib import net


class ShortestPath(ABC):
    def __init__(self, net, weights):
        """
        Abstract class for shortest path algorithm implementations.

        :param net: sumolib.net instance
        :param dict weights: A dictionary mapping edge ids (string) to weights (double)
        """
        self.net = net
        self.weights = {}

        # Initialize all edge weights to 1
        for key in self.net.getEdges():
            for lane in key.getLanes():
                self.weights[lane] = 1

        # Set edge weights passed
        for key in weights:
            self.weights[key] = weights[key]

    @abstractmethod
    def run(self, start_edge_id, end_edge_id):
        """
        Return tuple ([<list of edges for calculated route>], <effort>).

        :param string start_edge_id: The id of the edge where the shortest path should start
        :param string end_edge_id: The id of the edge where the shortest path should end
        """
        assert self.net.hasEdge(start_edge_id) and self.net.hasEdge(end_edge_id), "start and end node must be part of the network"

    def set_weights(self, weights):
        """
        Sets the weights of multiple edges in the graph.

        :param weights: A dictionary mapping edge ids (string) to weights (double)
        """
        for edge_id, edge_weight in weights.items():
            self.weights[edge_id] = edge_weight

    def set_weight(self, edge, weight):
        """
        Sets the weight of a single edge in the graph.

        :param string edge: The id of edge to be updated
        :param double weight: The weight to update the edge to
        """
        self.weights[edge] = weight
