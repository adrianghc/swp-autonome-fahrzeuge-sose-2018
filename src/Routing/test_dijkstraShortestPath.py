from unittest import TestCase
from sumolib.net import Net
from Routing.DijkstraShortestPath import DijkstraShortestPath


class TestDijkstraShortestPath(TestCase):
    """
    A test for the Dijkstra shortest path algorithm implementation in `DijkstraShortestPath`.
    """

    def test_run(self):
        """
        Runs the test.
        """
        net = Net()
        def empty():
            pass
        name = "edge"
        prio = 0

        net.addNode("node1")
        net.addNode("node2")
        net.addNode("node3")
        net.addNode("node4")
        net.addNode("node5")
        net.addNode("node6")

        net.addEdge("edge1_2", "node1", "node2", prio, empty(), name)
        net.addEdge("edge1_3", "node1", "node3", prio, empty(), name)
        net.addEdge("edge1_6", "node1", "node6", prio, empty(), name)
        net.addEdge("edge2_3", "node2", "node3", prio, empty(), name)
        net.addEdge("edge2_4", "node2", "node4", prio, empty(), name)
        net.addEdge("edge3_4", "node3", "node4", prio, empty(), name)
        net.addEdge("edge3_6", "node3", "node6", prio, empty(), name)
        net.addEdge("edge4_5", "node4", "node5", prio, empty(), name)
        net.addEdge("edge6_5", "node6", "node5", prio, empty(), name)

        weights = {"edge1_2": 7, "edge1_3": 9, "edge1_6": 14, "edge2_3": 10, "edge2_4": 15, "edge3_4": 11, "edge3_6": 2,
                   "edge4_5": 6, "edge6_5": 9}

        instance = DijkstraShortestPath(net, weights)
        shortest_path = instance.run("node1", "node5")
        assert shortest_path == ["node1", "node3", "node6", "node5"]
