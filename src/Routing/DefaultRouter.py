import helpers
import numpy as np

from Routing.Router import Router
from Simulation import SimulationManagement
from traci import vehicle, lane


class DefaultRouter(Router):

    def __init__(self, vehicle_id, sp_algorithm):
        """
        Default implementation of `Router`.
        """
        super().__init__(vehicle_id, sp_algorithm)
        net = SimulationManagement.graph_net
        weights = DefaultRouter._get_initial_weights().copy()
        self.sp = sp_algorithm(net, weights)

    def _get_initial_weights(weights={}):
        net = SimulationManagement.graph_net

        if weights:
            return weights

        for edge in net.getEdges():
            edge_id = edge.getID()
            # Expected time to traverse the edge
            tau = edge.getLength() / edge.getSpeed()
            weights[edge_id] = tau

        return weights

    def reroute(self, issue_objects, route):
        assert len(route) >= 2

        start_edge_id, end_edge_id = route[0], route[-1]
        net = SimulationManagement.graph_net
        self.sp.weights = DefaultRouter._get_initial_weights().copy()

        # Set weights for issue edges and edges nearby (within `radius` distance, with decayed weight)
        for issue in issue_objects:
            edge = net.getEdge(issue.edge)

            # Specifies the radius within which events are still relevant
            radius = 50
            nearby_edges = net.getNeighboringEdges(*issue.event_location, r=radius)
            # Tau for the issue edge
            tau = edge.getLength() / (issue.avg_speed + np.nextafter(0, 1))

            for edge, distance in nearby_edges:
                edge_id = edge.getID()
                edge_weight = self.sp.weights[edge_id]

                edge_weight_new = max(edge_weight, tau * (1 - distance / radius))
                self.sp.weights[edge_id] = edge_weight_new

        ########################################################################
        # Check if we are allowed to make a u-turn.
        ########################################################################

        reversed_start_edge_id = helpers.get_reversed_edge(start_edge_id)
        start_edge = net.getEdge(start_edge_id)
        u_turn_allowed = False

        try:
            reversed_start_edge = net.getEdge(reversed_start_edge_id)
            next_node_from_start = start_edge.getToNode()
            connections_on_next_node = next_node_from_start.getConnections()
            connections_from_start = list(filter(lambda x: x.getFrom() == start_edge, connections_on_next_node))
            connection_destinations = list(map(lambda x: x.getTo(), connections_from_start))
            u_turn_allowed = reversed_start_edge in connection_destinations
        except KeyError:
            # there is no reverse edge
            pass

        ########################################################################
        # Calculate new route. If we are allowed to do a u-turn, consider
        # doing it straight away and use the reversed edge as start. This makes
        # sense in real-life. In sumo, it's kind of a hack, as one needs
        # to drive until the end of the edge anyways. Though it proves to
        # work better for our scenario.
        ########################################################################

        shortest_path, effort = self.sp.run(start_edge_id, end_edge_id)
        shortest_path_u, effort_u = [], np.inf

        if u_turn_allowed:
            shortest_path_u, effort_u = self.sp.run(reversed_start_edge_id, end_edge_id)

        if effort_u < effort:
            # hack, as SUMO allows u-turns only at junctions
            new_route = [start_edge_id] + shortest_path_u
            vehicle.setRoute(self.vid, new_route)
        else:
            vehicle.setRoute(self.vid, shortest_path)
